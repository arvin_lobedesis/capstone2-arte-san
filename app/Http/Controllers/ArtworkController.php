<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Artwork;
use \App\Category;
use \App\User;
use Auth;
use Session;

class ArtworkController extends Controller
{
    public function index(){
    	$artworks = Artwork::all();
    	$categories = Category::all();

    	return view('/index', compact('artworks','categories'));
    }
    public function create(){
		$categories = Category::all();

		return view('userviews.addartwork', compact('categories'));
	}
    public function store(Request $req){
		// validate

		$rules = array(
			"title" => "required",
			"description" => "required",
			"category_id" => "required",
			"mediaPath" => "required|file|mimes:jpeg,jpg,png,gif,tiff,tif,mp4,mov,ogg"
		);

		$this->validate($req, $rules);

		// dd($req);
		// capture


		$newArtwork = new Artwork;
		$newArtwork->title = $req->title;
		$newArtwork->description = $req->description;
		$newArtwork->category_id = $req->category_id;
		$newArtwork->user_id = Auth::user()->id;

		// image handling
		$file = $req->file('mediaPath');
		// we'll rename the image
		$file_name = time().".".$file->getClientOriginalExtension();

		$destination = "files/"; //corresponds to the public files directory

		$file->move($destination, $file_name);

		$newArtwork->mediaPath = $destination.$file_name;
		// save
		$newArtwork->save();
		Session::flash("message","$newArtwork->name has been added");
		
		// redirect

		return redirect('/profile');

	}
}
