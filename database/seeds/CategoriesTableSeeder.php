<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert(
        	array(
        		0=>array(
        			'id'=>1,
        			'name'=>'Animation',
        			'created_at'=>NULL,
        			'updated_at'=>NULL
        		),
        		1=>array(
        			'id'=>2,
        			'name'=>'Crafts',
        			'created_at'=>NULL,
        			'updated_at'=>NULL
        		),
        		2=>array(
        			'id'=>3,
        			'name'=>'Digital Art',
        			'created_at'=>NULL,
        			'updated_at'=>NULL
        		),
        		3=>array(
        			'id'=>4,
        			'name'=>'Photography',
        			'created_at'=>NULL,
        			'updated_at'=>NULL
        		),
        		4=>array(
        			'id'=>5,
        			'name'=>'Traditional',
        			'created_at'=>NULL,
        			'updated_at'=>NULL
        		)
        	)
        );
    }
}
