<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
//show index
Route::get('/index','ArtworkController@index');
// show profile
Route::get('/profile','UserController@profilePage');

// add artwork
// show add item category option
Route::get('/addartwork','ArtworkController@create');
// to save
Route::post('/addartwork','ArtworkController@store');


