@extends('layouts.app')
@section('content')
<h1 class="text-center py-3">Add Artwork</h1>
@if(count($errors)>0)
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>

	@endforeach
@endif
<div class="col-lg-6 offset-lg-3">
	<form action="/addartwork" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="mediaPath">Choose File: </label>
			<input type="file" name="mediaPath" class="form-control" style="height: 50px;">
		</div>
		<div class="form-group">
			<label for="title">Title: </label>
			<input type="text" name="title" class="form-control">
		</div>
		<div class="form-group">
			<label for="description">Description: </label>
			<textarea name="description" id="desc" rows="3" class="form-control"></textarea>
		</div>
		
		
		<div class="form-group">
			<label for="category_id">Category:</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{$category->id}}">{{$category->name}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary" type="submit">Add Artwork</button>
	</form>
</div>
@endsection