@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		@foreach($users as $user)
		<div class="d-flex justify-content-start my-5">
			<div class="picborder">
				<img class="image2" src="{{asset('files/dp.png')}}" alt="" width="100%">
				<div class="mid2"><span class="btn" data-toggle="modal" data-target="#picModal">Change</span></div>
			</div>
			<h1 class="px-2">{{$user->username}}</h1>
		</div>
		@endforeach
	</div>
	<div class="modal fade" id="picModal">
	  <div class="modal-dialog" role="dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Upload Avatar</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="" method="POST">
	        	@csrf
	        	<div class="form-group">
					<label for="imgPath">Choose File: </label>
					<input type="file" name="imgPath" class="form-control" style="height: 50px;">
				</div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary">Upload</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

</div>
<div class="container">
	<ul class="nav nav-tabs">
	 
	  <li class="nav-item">
	    <a class="nav-link " data-toggle="tab" href="#profile">Profile</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link active" data-toggle="tab" href="#gallery">Gallery</a>
	  </li>
	  
	</ul>
	<div id="myTabContent" class="tab-content">
	  
	  <div class="tab-pane fade my-3" id="profile">
	    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
	  </div>
	  <div class="tab-pane fade active show" id="gallery">
			<div class="d-flex justify-content-end">
				<a href="/addartwork" class="btn btn-primary my-3">Add Artwork</a>
			</div>
			<div class="row">
				@foreach($artworks as $artwork)
				<div class="col-lg-3 my-3 py-1 con">
					<a href=""><img class="card-img-top image" src="{{asset($artwork->mediaPath)}}"></a>
					<div class="card mid">
						<div class="card-body">
							<h5 class="card-title font-weight-bold">{{$artwork->title}}</h5>
							<p class="card-title">by: {{$artwork->user->username}}</p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		
	    
	  </div>
	  
	</div>
	
</div>
{{-- <div class="container">
	
	<div class="d-flex justify-content-end">
		<a href="/addartwork" class="btn btn-primary">Add Artwork</a>
	</div>
	
	<div class="col-lg-12">
		<div class="row">
			@foreach($artworks as $artwork)
			<div class="col-lg-3 my-3 py-1 con">
				<img class="card-img-top image" src="{{asset($artwork->mediaPath)}}">
				<div class="card mid">
					<div class="card-body">
						<h5 class="card-title font-weight-bold">{{$artwork->title}}</h5>
						<p class="card-title">by: {{$artwork->user->username}}</p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	
</div> --}}

@endsection