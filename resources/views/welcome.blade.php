<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Art&#232-san</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        
    </head>
    <body>
      
        <div class="d-flex justify-content-center align-items-center vh-100 collapse">
            <h1 class="text-center py-5 text-white" id="head1">Welcome to</h1>
            <img src="{{asset('files/artesan.png')}}" alt="" id="artesan" class="img-fluid">
            <img src="{{asset('files/source.gif')}}" alt="" id="artesan-bg" class="img-fluid">
            <h5 class="text-center py-5 text-white" id="head2"><a href="{{ url('/index') }}" class="h4 font-weight-bold">Join us!</a> 
                <br>Let's make the world a better place.</h5>
           

           

        </div>
        
        

    </body>
</html>
