@extends('layouts.app')
@section('content')


	
		<div class="col-lg-12">
			<div class="row">
				@foreach($artworks as $artwork)
				<div class="col-lg-3 my-3 py-1 con">
					<a href=""><img class="card-img-top image" src="{{asset($artwork->mediaPath)}}"></a>
					<div class="card mid">
						<div class="card-body">
							<h5 class="card-title font-weight-bold">{{$artwork->title}}</h5>
							<p class="card-title">by: {{$artwork->user->username}}</p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	

@endsection